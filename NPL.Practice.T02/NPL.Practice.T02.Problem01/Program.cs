﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Content: ");
string content = Console.ReadLine().Trim();
int maxLenght;
do
{
    try
    {
        Console.WriteLine("Max length: ");
        maxLenght = int.Parse(Console.ReadLine());
        break;
    } catch (Exception e)
    {
        Console.WriteLine("Enter number please!");
    }
} while (true);
Console.WriteLine("Summary: " + GetArticleSummary(content, maxLenght));

static string GetArticleSummary(string content, int maxLength){
    if(content.Length <= maxLength)
    {
        return content.Trim() + "...";
    }
    if (content[maxLength-1] == ' ')
    {
        return content.Substring(0, maxLength-1).Trim() + "...";
    }
    if (content[maxLength-1] != ' ' && content[maxLength] == ' ')
    {
        return content.Substring(0, maxLength).Trim() + "...";
    }
    if (content[maxLength - 1] != ' ' && content[maxLength] != ' ')
    {
        for(int i = maxLength - 1; i >= 0; i--)
        {
            if (content[i] == ' ')
            {
                return content.Substring(0, i).Trim() + "...";
            }
        }
    }
    return "...";
}

