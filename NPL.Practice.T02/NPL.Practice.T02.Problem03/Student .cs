﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    internal class Student : IGraduate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateOnly StartDate { get; set; }
        public decimal SQLMark { get; set; }
        public decimal CsharpMark { get; set; }
        public decimal DsaMark { get; set; }
        public decimal GPA { get; set; }
        public GraduateLevel GraduateLevel { get; set; }

        public Student(int id, string name, DateOnly startDate, decimal sqlMark, decimal csharpMark, decimal dsaMark)
        {
            Id = id;
            Name = name;
            StartDate = startDate;
            SQLMark = sqlMark;
            CsharpMark = csharpMark;
            DsaMark = dsaMark;
        }
        public void Graduate()
        {

            GPA = (SQLMark + CsharpMark + DsaMark) / 3;

            // Set GraduateLevel
            if (GPA >= 9)
            {
                GraduateLevel = GraduateLevel.Excellent;
            }
            else if (GPA >= 8)
            {
                GraduateLevel = GraduateLevel.VeryGood;
            }
            else if (GPA >= 7)
            {
                GraduateLevel = GraduateLevel.Good;
            }
            else if (GPA >= 5)
            {
                GraduateLevel = GraduateLevel.AverageGood;
            }
            else
            {
                GraduateLevel = GraduateLevel.Failed;
            }
        }
        public string GetCertificate()
        {
            return String.Format("{0, -5}|{1, -25}|{2, -15}|{3, -15}|{4, -15}|{5, -10}|{6, -15}|{7, -10}", Id, Name, StartDate.ToString("dd/MM/yyyy"), SQLMark, CsharpMark, DsaMark, GPA.ToString("##.##"), GraduateLevel);
        }
    }

}

