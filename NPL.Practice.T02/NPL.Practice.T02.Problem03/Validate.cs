﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    public class Validate
    {
        public static int GetValidIntInput(string prompt, string errorMessage)
        {
            int result;
            while (true)
            {
                Console.Write(prompt);
                if (int.TryParse(Console.ReadLine(), out result))
                {
                    return result;
                }
                Console.WriteLine(errorMessage);
            }
        }

        public static decimal GetValidDecimalInput(string prompt, string errorMessage, decimal minValue, decimal maxValue)
        {
            decimal result;
            while (true)
            {
                Console.Write(prompt);
                if (decimal.TryParse(Console.ReadLine(), out result) && result >= minValue && result <= maxValue)
                {
                    return result;
                }
                Console.WriteLine(errorMessage);
            }
        }

        public static string GetNonEmptyStringInput(string prompt, string errorMessage)
        {
            string result;
            while (true)
            {
                Console.Write(prompt);
                result = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(result))
                {
                    return result;
                }
                Console.WriteLine(errorMessage);
            }
        }

        public static DateOnly GetValidDateInput(string prompt, string errorMessage)
        {
            DateOnly result;
            while (true)
            {
                Console.Write(prompt);
                if (DateOnly.TryParseExact(Console.ReadLine(), "dd/MM/yyyy", out result))
                {
                    return result;
                }
                Console.WriteLine(errorMessage);
            }
        }
    }
}
