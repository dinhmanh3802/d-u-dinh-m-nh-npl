﻿// See https://aka.ms/new-console-template for more information

using NPL.Practice.T02.Problem03;
using System.ComponentModel.DataAnnotations;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;

Validate validate = new Validate();
List<Student> students = new List<Student>();
bool exit = false;
while (!exit)
{
    Console.WriteLine("----- Student Management System -----");
    Console.WriteLine("1. Add Student");
    Console.WriteLine("2. List Students");
    Console.WriteLine("0. Exit");
    Console.Write("Enter your choice: ");

    string choice = Console.ReadLine();
    switch (choice)
    {
        case "1":
            AddStudent(students);
            break;
        case "2":
            ListStudents(students);
            break;
        case "0":
            exit = true;
            break;
        default:
            Console.WriteLine("Invalid choice. Please try again.");
            break;
    }
}
static void AddStudent(List<Student> students)
{
    int id = Validate.GetValidIntInput("Enter Student ID: ", "Invalid input. Please enter a valid integer for Student ID.");
    while (students.Any(student => student.Id == id))
    {
        Console.WriteLine("Student with the same ID already exists. Please enter a unique ID.");
        id = Validate.GetValidIntInput("Enter Student ID: ", "Invalid input. Please enter a valid integer for Student ID.");
    }

    string name = Validate.GetNonEmptyStringInput("Enter Student Name: ", "Name cannot be empty. Please enter a valid name.");
    DateOnly startDate = Validate.GetValidDateInput("Enter Start Date (dd/MM/yyyy): ", "Invalid date format. Please enter a valid date in the format dd/MM/yyyy.");
    decimal sqlMark = Validate.GetValidDecimalInput("Enter SQL Mark (0-10): ", "Invalid input. Please enter a valid decimal between 0 and 10 for SQL Mark.", 0, 10);
    decimal csharpMark = Validate.GetValidDecimalInput("Enter C# Mark (0-10): ", "Invalid input. Please enter a valid decimal between 0 and 10 for C# Mark.", 0, 10);
    decimal dsaMark = Validate.GetValidDecimalInput("Enter DSA Mark (0-10): ", "Invalid input. Please enter a valid decimal between 0 and 10 for DSA Mark.", 0, 10);

    Student student = new Student(id, name, startDate, sqlMark, csharpMark, dsaMark);
    student.Graduate();

    students.Add(student);
    Console.WriteLine("Student added successfully!");
}




static void ListStudents(List<Student> students)
{
    if (students.Count == 0)
    {
        Console.WriteLine("No students in the list.");
    }
    else
    {
        Console.WriteLine("----- Student List -----");
        Console.WriteLine("{0, -5}|{1, -25}|{2, -15}|{3, -15}|{4, -15}|{5, -10}|{6, -15}|{7, -10}", "ID", "Name", "Start Date", "SQL Mark", "C# Mark", "DSA Mark", "GPA", "Graduate Level");
        foreach (var student in students)
        {
            Console.WriteLine(student.GetCertificate());
        }
    }
}




