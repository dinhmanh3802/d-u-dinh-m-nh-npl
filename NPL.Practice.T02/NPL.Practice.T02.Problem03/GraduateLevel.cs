﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    internal enum GraduateLevel
    {
        Excellent,
        VeryGood,
        Good,
        AverageGood,
        Failed
    }

}
