﻿// See https://aka.ms/new-console-template for more information

int numberOfElement = 0;
do
{
    try
    {
        Console.Write("Enter number of elements: ");
        numberOfElement = int.Parse(Console.ReadLine());
    }
    catch (Exception e)
    {
        Console.WriteLine("Enter number please!");
    }
} while (numberOfElement <= 0);

int[] arr = new int[numberOfElement];

for (int i = 0; i < numberOfElement; i++)
{
    do
    {
        try
        {
            Console.Write("arr[" + i + "]: ");
            arr[i] = int.Parse(Console.ReadLine());
            break;
        }
        catch (Exception e)
        {
            Console.WriteLine("Enter number please!");
        }
    } while (true);
}
int subLength = 0;
do
{
    try
    {
        Console.Write("Enter the sub length: ");
        subLength = int.Parse(Console.ReadLine());
    }
    catch (Exception e)
    {
        Console.WriteLine("Enter number please!");
    }
} while (subLength <= 0);
Console.WriteLine("Max contiguous subarray: " + FindMaxSubArray(arr, subLength));

static int FindMaxSubArray(int[] inputArray, int subLength)
{
    int[] sumarr = new int[inputArray.Length - subLength + 1];
    for (int i = 0; i <= inputArray.Length - subLength; i++)
    {
        for (int j = i; j < i + subLength; j++)
        {
            sumarr[i] += inputArray[j];
        }
    }
    return sumarr.Max();
}
