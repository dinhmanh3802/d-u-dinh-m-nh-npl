﻿// See https://aka.ms/new-console-template for more information

static int[] RemoveDuplicate(int[] array)
{
    if (array == null || array.Length == 0)
    {
        return array;
    }

    List<int> newList = new List<int>();

    foreach (int item in array)
    {
        if (!newList.Contains(item))
        {
            newList.Add(item);
        }
    }

    return newList.ToArray();
}

static T[] RemoveDuplicate2<T>(T[] array)
{
    if (array == null || array.Length == 0)
    {
        return array;
    }

    List<T> newList = new List<T>();

    foreach (T item in array)
    {
        if (!newList.Contains(item))
        {
            newList.Add(item);
        }
    }

    return newList.ToArray();
}

Console.WriteLine("OUTPUT: ");

int[] intArray = new int[] { 1, 2, 3, 3, 5, 6, 5, 2 };
Console.WriteLine("Integer Array: " + string.Join(", ", intArray));
int[] distinctIntArray = RemoveDuplicate(intArray);
Console.WriteLine("Interger Array after Distinct: " + string.Join(", ", distinctIntArray));

Console.WriteLine();

string[] stringArray = new string[] { "Hung", "Vu", "Van", "Hung", "Quang", "Huy", "Vu" };
Console.WriteLine("String Array: " + string.Join(", ", stringArray));
string[] distinctStringArray = RemoveDuplicate2(stringArray);
Console.WriteLine("String Array after Distinct: " + string.Join(", ", distinctStringArray));