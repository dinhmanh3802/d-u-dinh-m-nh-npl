﻿// See https://aka.ms/new-console-template for more information



static int LastIndexOf<T>(T[] array, T elementValue)
{
    for (int i = array.Length - 1; i >= 0; i--)
    {
        if (object.Equals(array[i], elementValue))
        {
            return i;
        }
    }
    return -1;
}

Console.WriteLine("- TEST CASE 1: ");
int[] intArray = new int[] { 1, 2, 3, 5, 7, 3, 2 };
Console.WriteLine("Integer Array: " + string.Join(", ", intArray));
int elementValue1 = 3;
int result1 = LastIndexOf(intArray, elementValue1);
Console.WriteLine("Element value: " + elementValue1);
Console.WriteLine("The last index of " + elementValue1 + " is " + result1);

Console.WriteLine("");

Console.WriteLine("- TEST CASE 2: ");
string[] stringArray = new string[] { "Hoang", "Trong", "A", "Hoang", "Trong", "Hieu" };
Console.WriteLine("String Array: " + string.Join(", ", stringArray));
string elementValue2 = "Trong";
int result2 = LastIndexOf(stringArray, elementValue2);
Console.WriteLine("Element value: " + elementValue2);
Console.WriteLine("The last index of " + elementValue2 + " is " + result2);