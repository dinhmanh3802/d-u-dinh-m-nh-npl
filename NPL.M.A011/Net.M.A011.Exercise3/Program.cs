﻿// See https://aka.ms/new-console-template for more information

static int ElementOfOrder2(int[] array)
{
    if (array.Length < 2)
    {
        throw new InvalidOperationException("The array does not have a 2nd largest element.");
    }
    int max= array[0];
    int nextMax = array[1];
    if (max < nextMax)
    {
        int temp = nextMax; 
        nextMax = max;
        max = temp;
    }
    for (int i = 2; i < array.Length; i++)
    {
        if (array[i] > nextMax && array[i] != max)
        {
            nextMax = array[i];
            if (nextMax > max)
            {
                int temp = nextMax;
                nextMax = max;
                max = temp;
            }
        }
    }
    return nextMax;
}


static T ElementOfOrder<T>(T[] array, int orderLargest)
{
    if (orderLargest < 1 || orderLargest > array.Length)
    {
        throw new Exception("Invalid orderLargest.");
    }
    if (!typeof(T).IsPrimitive && typeof(T) != typeof(string))
    {
        throw new Exception("Invalid type.");
    }
    Array.Sort(array);
    T[] distinctArray = array.Distinct().ToArray();
    Array.Sort(distinctArray);
    return distinctArray[distinctArray.Length - orderLargest];
}


Console.WriteLine("OUTPUT: ");
int[] intArray = new int[] { 3, 2, 5, 6, 1, 7, 7, 5, 2};
Console.WriteLine("Integer Array: " + string.Join(", ", intArray));

Console.WriteLine("Element of Order 2: " + ElementOfOrder2(intArray));
Console.WriteLine("Element of Order 5: " + ElementOfOrder(intArray, 5));

//Test trường hợp lỗi
Console.Write("[ERROR CASE] - ");
try
{
    Console.Write("Element of Order 20: ");
    Console.WriteLine(ElementOfOrder(intArray, 20));
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}