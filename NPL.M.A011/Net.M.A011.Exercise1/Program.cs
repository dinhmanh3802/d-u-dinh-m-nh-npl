﻿// See https://aka.ms/new-console-template for more information

using System.Collections;

static int CountInt(ArrayList array)
{
    return array.OfType<int>().Count();
}

static int CountOf(ArrayList array, Type dataType)
{
    return array.Cast<object>().Count(item => dataType.IsInstanceOfType(item));
}

//không hiểu sao nó không cho đặt trùng tên???
static int CountOf1<T>(ArrayList array)
{
    int count = 0;

    foreach (var element in array)
    {
        if (element is T)
        {
            count++;
        }
    }
    return count;
}

static T MaxOf<T>( ArrayList array)
{
    if (IsNumericType(typeof(T)))
    {
        var numericValues = array.OfType<T>();
        if (numericValues.Any())
        {
            return numericValues.Max();
        }
        else
        {
            throw new InvalidOperationException("The ArrayList does not contain any elements of type " + typeof(T).Name);
        }
    }
    else
    {
        throw new InvalidOperationException("The type " + typeof(T).Name + " is not a numeric data type.");
    }
}

static bool IsNumericType(Type type)
{
    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
    {
        type = Nullable.GetUnderlyingType(type);
    }

    return type == typeof(byte) || type == typeof(sbyte) || type == typeof(short) || type == typeof(ushort) ||
           type == typeof(int) || type == typeof(uint) || type == typeof(long) || type == typeof(ulong) ||
           type == typeof(float) || type == typeof(double) || type == typeof(decimal);
}



ArrayList arrayList = new ArrayList();
arrayList.Add("Hieu");
arrayList.Add("Hoang");
arrayList.Add(1);
arrayList.Add(2);
arrayList.Add("Trong");
arrayList.Add(3.9d);

Console.WriteLine("OUTPUT: ");

Console.Write("Count of int: " );
Console.WriteLine(CountInt(arrayList));

Console.Write("Count of typeof(int): ");
Console.WriteLine(CountOf(arrayList, typeof(int)));

Console.Write("Count of string: ");
Console.WriteLine(CountOf1<string>(arrayList));

Console.Write("Max of int: ");
Console.WriteLine(MaxOf<int>(arrayList));

Console.ReadLine();


