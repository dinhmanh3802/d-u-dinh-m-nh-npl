﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class Employee
    {
        public int employeeId { get; set; }
        public string employeeName { get; set; }
        public int employeeAge { get; set; }
        public string employeeAddress { get; set; }

        public string employeeHiredDate { get; set;}

        public string employeeStatus { get; set; }

        public int departmentId { get; set; }
    }
}
