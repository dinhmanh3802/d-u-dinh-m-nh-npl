﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class Management
    {
        List<Employee> employees = new List<Employee>();
        List<Department> departments = new List<Department>();
        List<ProgramingLanguage> programingLanguages = new List<ProgramingLanguage>();
        List<LanguageManagement> languageManagements = new List<LanguageManagement>();
        public void InputDepartmentInfo()
        {

            do
            {
                Department department = new Department();
                Console.WriteLine("Enter Department Information:");

                do
                {
                    Console.Write("Department ID: ");
                    int departmentId = int.Parse(Console.ReadLine());
                    if (departments.Any(dept => dept.departmentId == departmentId))
                    {
                        Console.WriteLine("Department ID already exists!");
                    }
                    else
                    {
                        department.departmentId = departmentId;
                        break;
                    }
                } while (true);

                Console.Write("Department Name: ");
                department.departmentName = Console.ReadLine();
                departments.Add(department);

                while (true)
                {
                    Console.Write("Do you want to enter another department? (y/n): ");
                    string response = Console.ReadLine().ToLower();
                    if (response == "y")
                    {
                        break; // Người dùng muốn tiếp tục, thoát khỏi vòng lặp này
                    }
                    else if (response == "n")
                    {
                        return; // Người dùng không muốn tiếp tục, kết thúc hàm
                    }
                    else
                    {
                        Console.WriteLine("Invalid input. Please enter 'y' or 'n'.");
                    }
                }
            } while (true);
        }


        public void InputLanguageInfo()
        {
            do
            {
                ProgramingLanguage programingLanguage = new ProgramingLanguage();
                Console.WriteLine("Enter Programming Language Information:");

                do
                {
                    Console.Write("Language ID: ");
                    int languageId = int.Parse(Console.ReadLine());
                    if (programingLanguages.Any(lang => lang.languageId == languageId))
                    {
                        Console.WriteLine("Language ID already exists!");
                    }
                    else
                    {
                        programingLanguage.languageId = languageId;
                        break;
                    }
                } while (true);

                Console.Write("Language Name: ");
                programingLanguage.languageName = Console.ReadLine();
                programingLanguages.Add(programingLanguage);

                while (true)
                {
                    Console.Write("Do you want to enter another programming language? (Y/N): ");
                    string continueInput = Console.ReadLine().Trim();
                    if (continueInput.Equals("N", StringComparison.OrdinalIgnoreCase))
                    {
                        return;
                    }
                    else if (continueInput.Equals("Y", StringComparison.OrdinalIgnoreCase))
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Invalid input. Please enter 'Y' or 'N'.");
                    }
                }
            } while (true);
        }

        public void InputEmployeeInfo()
        {
            do
            {
                Employee employee = new Employee();
                Console.WriteLine("Enter Employee Information:");

                do
                {
                    Console.Write("Employee ID: ");
                    int employeeId = int.Parse(Console.ReadLine());
                    if (employees.Any(emp => emp.employeeId == employeeId))
                    {
                        Console.WriteLine("Employee ID already exists!");
                    }
                    else
                    {
                        employee.employeeId = employeeId;
                        break;
                    }
                } while (true);

                Console.Write("Employee Name: ");
                employee.employeeName = Console.ReadLine();

                Console.Write("Employee Age: ");
                employee.employeeAge = int.Parse(Console.ReadLine());

                Console.Write("Employee Address: ");
                employee.employeeAddress = Console.ReadLine();

                Console.Write("Hired Date: ");
                employee.employeeHiredDate = Console.ReadLine();

                Console.Write("Employee Status: ");
                employee.employeeStatus = Console.ReadLine();

                do
                {
                    Console.Write("Department ID: ");
                    int departmentId = int.Parse(Console.ReadLine());
                    if (departments.Any(dp => dp.departmentId == departmentId))
                    {
                        employee.departmentId = departmentId;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Department ID not exist!");
                    }
                } while (true);

                employees.Add(employee);

                while (true)
                {
                    Console.Write("Do you want to enter another employee? (Y/N): ");
                    string continueInput = Console.ReadLine().Trim();
                    if (continueInput.Equals("N", StringComparison.OrdinalIgnoreCase))
                    {
                        return;
                    }
                    else if (continueInput.Equals("Y", StringComparison.OrdinalIgnoreCase))
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Invalid input. Please enter 'Y' or 'N'.");
                    }
                }
            } while (true);
        }

        public void InputLanguageManagement()
        {
            do
            {
                LanguageManagement languageManagement = new LanguageManagement();
                Console.WriteLine("Enter Language Management Information:");

                do
                {
                    Console.Write("Employee ID: ");
                    int employeeId = int.Parse(Console.ReadLine());

                    if (employees.Any(emp => emp.employeeId == employeeId))
                    {
                        languageManagement.employeeId = employeeId;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Invalid employee ID. No employee with this ID exists.");
                    }
                } while (true);

                do
                {
                    Console.Write("Language ID: ");
                    int languageId = int.Parse(Console.ReadLine());

                    if (programingLanguages.Any(lang => lang.languageId == languageId))
                    {
                        if (!languageManagements.Any(lm => lm.employeeId == languageManagement.employeeId && lm.languageId == languageId))
                        {
                            languageManagement.languageId = languageId;
                            break;
                        }
                        else
                        {
                            Console.WriteLine("This combination of Employee ID and Language ID already exists. Please enter both IDs again.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid language ID. No language with this ID exists.");
                    }
                } while (true);

                languageManagements.Add(languageManagement);

                while (true)
                {
                    Console.Write("Do you want to enter another Language Management? (Y/N): ");
                    string continueInput = Console.ReadLine().Trim();
                    if (continueInput.Equals("N", StringComparison.OrdinalIgnoreCase))
                    {
                        return;
                    }
                    else if (continueInput.Equals("Y", StringComparison.OrdinalIgnoreCase))
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Invalid input. Please enter 'Y' or 'N'.");
                    }
                }
            } while (true);
        }

        public List<Department> GetDepartments(int numberOfEmployees)
        {
            var department = from dept in departments
                             where employees.Count(emp => emp.departmentId == dept.departmentId) >= numberOfEmployees
                             select dept;
            return department.ToList();
        }

        public List<Employee> GetEmployeesWorking()
        {
            var workingEmployees = employees.Where(emp => emp.employeeStatus.Equals("working", StringComparison.OrdinalIgnoreCase)).ToList();
            return workingEmployees;
        }

        public List<Employee> GetEmployees(string languageName)
        {
            var employeesKnowLanguage = (from emp in employees
                                         join lm in languageManagements on emp.employeeId equals lm.employeeId
                                         join lang in programingLanguages on lm.languageId equals lang.languageId
                                         where lang.languageName == languageName
                                         select emp).Distinct().ToList();
            return employeesKnowLanguage;
        }

        public List<ProgramingLanguage> GetLanguages(int employeeId)
        {
            var languagesKnownByEmployee = (from lm in languageManagements
                                            where lm.employeeId == employeeId
                                            join lang in programingLanguages on lm.languageId equals lang.languageId
                                            select lang).ToList();

            return languagesKnownByEmployee;
        }

        public List<Employee> GetSeniorEmployee()
        {
            var employeesWithMultipleLanguages = (from emp in employees
                                                  join lm in languageManagements on emp.employeeId equals lm.employeeId
                                                  join lang in programingLanguages on lm.languageId equals lang.languageId
                                                  group lang by emp into grouped
                                                  where grouped.Select(l => l.languageId).Distinct().Count() > 1
                                                  select grouped.Key).ToList();

            return employeesWithMultipleLanguages;
        }

        public List<Employee> GetEmployeePaging(int pageIndex = 1, int pageSize = 10, string employeeName = null, string order = "ASC")
        {
            var query = employees.AsEnumerable();

            if (!string.IsNullOrWhiteSpace(employeeName))
            {
                query = query.Where(emp => emp.employeeName.Contains(employeeName, StringComparison.OrdinalIgnoreCase));
            }

            if (order.Equals("DESC", StringComparison.OrdinalIgnoreCase))
            {
                query = query.OrderByDescending(emp => emp.employeeName);
            }
            else
            {
                query = query.OrderBy(emp => emp.employeeName);
            }

            var pagedEmployees = query.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            return pagedEmployees;
        }

        public void GetDepartments()
        {
            var departmentsWithEmployees = from department in departments
                                           join employee in employees on department.departmentId equals employee.departmentId into departmentEmployees
                                           select new
                                           {
                                               Department = department,
                                               Employees = departmentEmployees.ToList()
                                           };

            foreach (var item in departmentsWithEmployees)
            {
                Console.WriteLine($"Department ID: {item.Department.departmentId}");
                Console.WriteLine($"Department Name: {item.Department.departmentName}");
                Console.WriteLine("Employees in this Department:");

                foreach (var employee in item.Employees)
                {
                    Console.Write($"Employee ID: {employee.employeeId}");
                    Console.WriteLine($" & Employee Name: {employee.employeeName}");
                }

                Console.WriteLine("------------------------------");
            }
        }





    }
}
