﻿// See https://aka.ms/new-console-template for more information
using NPL.M.A015.Exercise;

Management management = new Management();
bool exit = false;
while (!exit)
{
    Console.WriteLine("Employee and Department Management System");
    Console.WriteLine("1. Add new Department");
    Console.WriteLine("2. Add new Programing Language");
    Console.WriteLine("3. Add new Employee");
    Console.WriteLine("4. Add new Programing Language Management");
    Console.WriteLine("5. List Departments with >= Number of Employees");
    Console.WriteLine("6. List All Working Employees");
    Console.WriteLine("7. List Employees by Programming Language");
    Console.WriteLine("8. List Programming Languages of an Employee");
    Console.WriteLine("9. List Senior Employees - Who Knows Multiple Programming Languages");
    Console.WriteLine("10. List Employees with Paging and Sorting");
    Console.WriteLine("11. List Departments with Employees");
    Console.WriteLine("0. Exit");

    Console.Write("Enter your choice: ");
    string choice = Console.ReadLine();

    switch (choice)
    {
        case "1":
            management.InputDepartmentInfo();
            break;
        case "2":
            management.InputLanguageInfo();
            break;
        case "3":
            management.InputEmployeeInfo();
            break;
        case "4":
            management.InputLanguageManagement();
            break;
        case "5":
            Console.Write("Enter the minimum number of employees: ");
            int minEmployees = int.Parse(Console.ReadLine());
            var departments = management.GetDepartments(minEmployees);
            Console.WriteLine("Departments with at least " + minEmployees + " employees:");
            foreach (var department in departments)
            {
                Console.WriteLine($"Department ID: {department.departmentId}, Department Name: {department.departmentName}");
            }
            break;
        case "6":
            var workingEmployees = management.GetEmployeesWorking();
            Console.WriteLine("Working Employees:");
            foreach (var employee in workingEmployees)
            {
                Console.WriteLine($"Employee ID: {employee.employeeId}, Employee Name: {employee.employeeName}");
            }
            break;
        case "7":
            Console.Write("Enter the language name: ");
            string languageName = Console.ReadLine();
            var employeesByLanguage = management.GetEmployees(languageName);
            Console.WriteLine("Employees who know " + languageName + ":");
            foreach (var employee in employeesByLanguage)
            {
                Console.WriteLine($"Employee ID: {employee.employeeId}, Employee Name: {employee.employeeName}");
            }
            break;
        case "8":
            Console.Write("Enter Employee ID: ");
            int employeeId = int.Parse(Console.ReadLine());
            var languagesKnown = management.GetLanguages(employeeId);
            Console.WriteLine("Languages known by Employee ID " + employeeId + ":");
            foreach (var language in languagesKnown)
            {
                Console.WriteLine($"Language ID: {language.languageId}, Language Name: {language.languageName}");
            }
            break;
        case "9":
            var seniorEmployees = management.GetSeniorEmployee();
            Console.WriteLine("Senior Employees who know multiple programming languages:");
            foreach (var employee in seniorEmployees)
            {
                Console.WriteLine($"Employee ID: {employee.employeeId}, Employee Name: {employee.employeeName}");
            }
            break;
        case "10":
            Console.Write("Enter page index: ");
            int pageIndex = int.Parse(Console.ReadLine());
            Console.Write("Enter page size: ");
            int pageSize = int.Parse(Console.ReadLine());
            Console.Write("Enter employee name (optional): ");
            string searchEmployeeName = Console.ReadLine();
            Console.Write("Enter order (ASC/DESC): ");
            string order = Console.ReadLine();
            var pagedEmployees = management.GetEmployeePaging(pageIndex, pageSize, searchEmployeeName, order);
            Console.WriteLine($"Paged Employees (Page {pageIndex}, Page Size {pageSize}, Order {order}):");
            foreach (var employee in pagedEmployees)
            {
                Console.WriteLine($"Employee ID: {employee.employeeId}, Employee Name: {employee.employeeName}");
            }
            break;
        case "11":
            management.GetDepartments();
            break;
        case "0":
            Console.WriteLine("Exiting the program.");
            exit = true;
            break;
        default:
            Console.WriteLine("Invalid choice. Please enter a valid option.");
            break;
    }

}

